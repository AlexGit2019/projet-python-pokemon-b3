from django.db import models
from pokemon.infrastructure.pokemon_api_storage import pokemonApiStorage
from pokemon.services.pokemon_service import pokemonService
from pokemon.domaine.equipe import equipe
# Create your models here.
class equipeModel(models.Model):
    nom = models.CharField(primary_key=True, max_length=255)
    pokemon_un = models.IntegerField(blank=True, null=True)
    pokemon_deux = models.IntegerField(blank=True, null=True)
    pokemon_trois = models.IntegerField(blank=True, null=True)
    pokemon_quatre = models.IntegerField(blank=True, null=True)
    pokemon_cinq = models.IntegerField(blank=True, null=True)

    def recuperer_pokemons(self):
        liste_pokemons = []
        pokemon_service = pokemonService(pokemonApiStorage())
        #Récupération du dictionnaire stockant les valeur des attibuts de l'objet equipe
        dictionnnaire_attributs_objet_equipe = self.__dict__
        liste_attributs_objet_equipe = list(dictionnnaire_attributs_objet_equipe.values())
        for i in range(2,len(liste_attributs_objet_equipe)):
            if (liste_attributs_objet_equipe[i] is not None):
                liste_pokemons.append(pokemon_service.recuperer_pokemon(liste_attributs_objet_equipe[i]))
        return liste_pokemons
    def mettre_id_pokemons_equipe_a_null(self):
        self.pokemon_un = None
        self.pokemon_deux = None
        self.pokemon_trois = None
        self.pokemon_quatre = None
        self.pokemon_cinq = None
    def mettre_a_jour_id_pokemons_equipe(self, objet_du_domaine_equipe):
        if objet_du_domaine_equipe.a_pokemon(1):
            self.pokemon_un = objet_du_domaine_equipe.liste_pokemons[0].numero_pokedex
            if objet_du_domaine_equipe.a_pokemon(2):
                self.pokemon_deux = objet_du_domaine_equipe.liste_pokemons[1].numero_pokedex
                if objet_du_domaine_equipe.a_pokemon(3):
                    self.pokemon_trois = objet_du_domaine_equipe.liste_pokemons[2].numero_pokedex
                    if objet_du_domaine_equipe.a_pokemon(4):
                        self.pokemon_quatre = objet_du_domaine_equipe.liste_pokemons[3].numero_pokedex
                        if(objet_du_domaine_equipe.a_pokemon(5)):
                            self.pokemon_cinq = objet_du_domaine_equipe.liste_pokemons[4].numero_pokedex


