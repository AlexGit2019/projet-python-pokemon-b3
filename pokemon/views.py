# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from pokemon.services.pokemon_service import pokemonService
from pokemon.infrastructure.pokemon_api_storage import pokemonApiStorage
from pokemon.infrastructure.equipe_django_model_storage import equipeDjangoModelStorage
from pokemon.infrastructure.pokemon_search_form import pokemonSearchForm
from pokemon.services.equipe_service import equipeService
from django.shortcuts import redirect
from pokemon.domaine.equipe import equipe

POSITION_PREMIER_POKEMON_DANS_LA_LISTE = 1
def redirect_to_home_page(request):
    return redirect('display_pokemon', position_dans_la_liste=POSITION_PREMIER_POKEMON_DANS_LA_LISTE)
def displayPokemon(request, position_dans_la_liste):

    # Appel de la factory method from_id_or_name de la classe pokemonFactory
    service_pokemon = pokemonService(pokemonApiStorage())
    pokemon = service_pokemon.recuperer_pokemon(position_dans_la_liste)
    taille_de_la_liste = service_pokemon.taille_de_la_liste()
    page_title = "Affichage du pokémon " + pokemon.nom
    pokemon_search_form = pokemonSearchForm()
    return render(request, "pokemon/pokemon_display page.html", {'pokemon': pokemon, 'page_title': page_title, "pokemon_search_form": pokemon_search_form, "position_dans_la_liste": position_dans_la_liste, "taille_de_la_liste": taille_de_la_liste}, status=200)
def listerEquipes(request):
    equipe_service = equipeService(equipeDjangoModelStorage(), pokemonService(pokemonApiStorage()))
    liste_objets_du_domaine_equipes = equipe_service.recuperer_equipes()
    return render(request, "pokemon/liste_des_equipes.html", {"liste_des_objets_du_domaine_equipes": liste_objets_du_domaine_equipes})

def listerEquipesPourAjout(request, id_pokemon):
    equipe_service = equipeService(equipeDjangoModelStorage(), pokemonService(pokemonApiStorage()))
    liste_objets_du_domaine_equipes = equipe_service.recuperer_equipes()
    page_title = "Liste des equipes :"
    return render(request, "pokemon/liste_des_equipes_pour_ajout.html", {"id_pokemon": id_pokemon,  "liste_des_objets_du_domaine_equipes": liste_objets_du_domaine_equipes, "page_title": page_title})


def afficherEquipe(request, nom_equipe):
    try:
        equipe_service = equipeService(equipeDjangoModelStorage(), pokemonService(pokemonApiStorage()))
        equipe_recuperee = equipe_service.recuperer_equipe(nom_equipe)
        page_title = "Affichage des pokémons de l'équipe " + equipe_recuperee.nom
        return render(request, "pokemon/team_detail_display_page.html", {'page_title': page_title, 'equipe_recuperee': equipe_recuperee})
    except Exception as e:
        return HttpResponse()

def ajouterPokemonAEquipe(request):
    donnees_requete_http_post = request.POST
    id_pokemon = int(donnees_requete_http_post.__getitem__('id_pokemon'))
    nom_equipe = donnees_requete_http_post.__getitem__('equipe')
    pokemon_service = pokemonService(pokemonApiStorage())
    equipe_service = equipeService(equipeDjangoModelStorage(),pokemon_service)
    try:
        equipe_service.ajouter_pokemon_a_equipe(nom_equipe, id_pokemon)
    except Exception as e:
        raise e
    return HttpResponse()

def searchPokemon(request):
    pokemon_service = pokemonService(pokemonApiStorage())
    try:
        form = pokemonSearchForm(request.GET)
        if (form.is_valid()):
            nom_pokemon = form.cleaned_data['nom_pokemon']
            pokemon_recupere = pokemon_service.rechercher_pokemon(nom_pokemon)
            page_title = "Affichage du pokémon " + pokemon_recupere.nom
            pokemon_search_form = pokemonSearchForm()
            return render(request, "pokemon/pokemon_search_result_display page.html", {"page_title": page_title, "pokemon_search_form": pokemon_search_form, "pokemon": pokemon_recupere})
    except Exception:
        return HttpResponse()
    return HttpResponse()