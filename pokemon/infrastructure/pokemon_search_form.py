from django import forms

class pokemonSearchForm(forms.Form):
    nom_pokemon = forms.CharField(widget=forms.TextInput(attrs={'placeholder': '🔍 Rechercher un Pokémon...'}))