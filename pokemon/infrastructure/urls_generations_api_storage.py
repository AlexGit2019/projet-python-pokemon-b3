import requests
from pokemon.domaine.Generation import Generation
class urlsGenerationApiStorage:
    dictionnaire_url_especes = {}
    @staticmethod
    def recuperer_url_generation(url_espece_pokemon):
        if (url_espece_pokemon in urlsGenerationApiStorage.dictionnaire_url_especes):
            return urlsGenerationApiStorage.dictionnaire_url_especes[url_espece_pokemon]
        else:
            response_espece = requests.get(url_espece_pokemon)
            reponse_espece_json = response_espece.json()
            url_generation_pokemon = reponse_espece_json['generation']['url']
            urlsGenerationApiStorage.dictionnaire_url_especes[url_espece_pokemon] = url_generation_pokemon
            return url_generation_pokemon


