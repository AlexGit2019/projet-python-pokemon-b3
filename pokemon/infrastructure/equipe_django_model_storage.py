import pokemon.services.equipe_storage_interface
from pokemon.models import equipeModel
from pokemon.domaine.client_exception import clientException
from pokemon.domaine.equipe import equipe
from django.db import transaction
class equipeDjangoModelStorage(pokemon.services.equipe_storage_interface.equipeStorageInterface):
    def recuperer_equipes(self):
        #Récupération de toutes les équipes depuis la BDD
        liste_objets_du_domaine_equipe = []
        query_set_equipes = equipeModel.objects.values("nom")
        for dict_equipe in query_set_equipes:
            nom_equipe = dict_equipe["nom"]
            liste_objets_du_domaine_equipe.append(equipe(nom_equipe))
        return liste_objets_du_domaine_equipe

    def recuperer_equipe(self, nom_equipe):
        try:
            manager_modele_equipe = equipeModel.objects
            equipe_modele_django_demandee = manager_modele_equipe.get(nom=nom_equipe)
            liste_pokemons_equipe = equipe_modele_django_demandee.recuperer_pokemons()
            return equipe(equipe_modele_django_demandee.nom,liste_pokemons_equipe)
        except equipeModel.DoesNotExist as does_not_exist_exception:
            raise clientException("L'équipe n'existe pas", does_not_exist_exception)
    @transaction.atomic
    def ajouter_pokemon_dans_equipe(self, objet_equipe_du_domaine):
        manager_equipes = equipeModel.objects
        objet_equipe_courante = manager_equipes.get(nom=objet_equipe_du_domaine.nom)
        objet_equipe_courante.mettre_id_pokemons_equipe_a_null()
        objet_equipe_courante.save()
        objet_equipe_courante.mettre_a_jour_id_pokemons_equipe(objet_equipe_du_domaine)
        objet_equipe_courante.save()

