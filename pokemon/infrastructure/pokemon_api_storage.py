import pokemon.services.pokemon_storage_interface
from pokemon.domaine.Pokemon import Pokemon
from pokemon.infrastructure.urls_generations_api_storage import urlsGenerationApiStorage
from pokemon.infrastructure.generation_api_storage import generationApiStorage
import requests
_base_url = "https://pokeapi.co/api/v2/"
_endpoint_pokemons = _base_url + "pokemon"
class pokemonApiStorage(pokemon.services.pokemon_storage_interface.equipeStorageInterface):
    # Redéfinition de méthode
    liste_tuples_urls_pokemons = []
    @staticmethod
    def recuperer_liste_url_pokemons():
        liste_pokemons = []
        # Recuperation du nombre de Pokémons :
        nombre_pokemons = requests.get(_endpoint_pokemons + "?limit=1").json()["count"]
        #Récupération de la liste de tous les Pokémons
        reponse_liste_pokemons = requests.get(_endpoint_pokemons + "?limit="+ str(nombre_pokemons))
        reponse_liste_pokemons_json = reponse_liste_pokemons.json()

        for pokemon_result_object in reponse_liste_pokemons_json["results"]:
            try:
                url = pokemon_result_object["url"]
                liste_pokemons.append((url, None))
            except IndexError:
                print(pokemon_result_object["name"])
        try:
            liste_pokemons.sort(key=lambda tuple_url_pokemon: int(tuple_url_pokemon[0].rsplit("/", 2)[1])) #Tri de la liste d'identifiants par rapport à l'identifiant unique de la ressource de l'API (l'ID du Pokémon)
        except Exception as e:
            print(e.__cause__)
        pokemonApiStorage.liste_tuples_urls_pokemons = liste_pokemons

    @staticmethod
    def from_url(url):
        response_pokemon = requests.get(url)
        json_pokemon = response_pokemon.json()
        numero_pokedex = int(json_pokemon['id'])
        nom_pokemon = json_pokemon['name']
        url_sprite_pokemon = json_pokemon['sprites']['front_default']
        poids_pokemon = json_pokemon['weight']
        url_espece_pokemon = json_pokemon['species']['url']
        # Récupération de l'URL servant à récupérer l'url servant à récupérer la generation de l'espèce du Pokémon
        url_generation_pokemon = urlsGenerationApiStorage.recuperer_url_generation(url_espece_pokemon)
        # Récupération de la génération du pokemon
        generation_pokemon = generationApiStorage.recuperer_generation(url_generation_pokemon)
        pokemon = Pokemon(nom_pokemon, url_sprite_pokemon, generation_pokemon, poids_pokemon, numero_pokedex)
        return pokemon


    def recuperer_pokemon(self, position_dans_la_liste):
        objet_pokemon_cible = pokemonApiStorage.liste_tuples_urls_pokemons[position_dans_la_liste - 1][1]
        if objet_pokemon_cible is None:
            pokemon = pokemonApiStorage.from_url(pokemonApiStorage.liste_tuples_urls_pokemons[position_dans_la_liste - 1][0])
            #Remplacement du tuple présent à cette position dans la liste
            pokemonApiStorage.liste_tuples_urls_pokemons[position_dans_la_liste - 1] = (pokemonApiStorage.liste_tuples_urls_pokemons[position_dans_la_liste - 1][0], pokemon)
            return pokemon
        else:
            return objet_pokemon_cible

    def rechercher_pokemon(self, nom_pokemon):
        url = _endpoint_pokemons + "/" + nom_pokemon
        try:
            pokemon_recupere = pokemonApiStorage.from_url(url)
            return pokemon_recupere
        except Exception:
            raise Exception
    def taille_de_la_liste(self):
        return len(pokemonApiStorage.liste_tuples_urls_pokemons)
    @staticmethod
    def calcul_cle_tri_items_liste_tuples_pokemons(item_liste):
        return

pokemonApiStorage.recuperer_liste_url_pokemons()