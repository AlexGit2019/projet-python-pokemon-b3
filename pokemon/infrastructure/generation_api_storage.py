import requests
from pokemon.domaine.Generation import Generation
class generationApiStorage:
    dictionnaire_generations = {}
    @staticmethod
    def recuperer_generation(url):
        if (url in generationApiStorage.dictionnaire_generations):
            return generationApiStorage.dictionnaire_generations[url]
        else:
            reponse_generation_pokemeon = requests.get(url)
            nom_generation_pokemon_fr = reponse_generation_pokemeon.json()['names'][2]['name']
            generation_pokemon = Generation(nom_generation_pokemon_fr)
            generationApiStorage.dictionnaire_generations[url] = generation_pokemon
            return generation_pokemon


