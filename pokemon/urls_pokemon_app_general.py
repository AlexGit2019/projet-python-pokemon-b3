from django.urls import path, include
from . import views
urlpatterns = [
    path('pokemon/display/<int:position_dans_la_liste>', views.displayPokemon, name="display_pokemon"),
    path('pokemon/search', views.searchPokemon, name="search_pokemon"),
    path('pokemon/add_to_team', views.ajouterPokemonAEquipe, name="add_to_team"),
    path('teams/', include('pokemon.urls_pokemon_app_teams'))
]