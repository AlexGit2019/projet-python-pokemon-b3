from pokemon.domaine.client_exception import clientException
class equipe :
    NOMBRE_POKEMONS_MAXIMAL_DANS_EQUIPE = 5
    def __init__(self,nom,liste_pokemons=None):
        self.nom = nom
        self.liste_pokemons = liste_pokemons
    def ajouter_pokemon_a_equipe(self, pokemon):
        if len(self.liste_pokemons) < equipe.NOMBRE_POKEMONS_MAXIMAL_DANS_EQUIPE:
            self.liste_pokemons.append(pokemon)
        else:
            raise clientException(message="L'équipe ne peut pas contenir plus de 5 Pokémons")
    def a_pokemon(self, numero_membre_equipe):
        return (numero_membre_equipe - 1) < len(self.liste_pokemons)
