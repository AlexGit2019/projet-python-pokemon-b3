class Pokemon:
    def __init__(self, nom, url_sprite, generation, poids, numero_pokedex):
        self.numero_pokedex = numero_pokedex
        self.nom = nom
        self.url_sprite = url_sprite
        self.generation = generation
        self.poids = poids
