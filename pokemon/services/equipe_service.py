from pokemon.infrastructure.pokemon_api_storage import pokemonApiStorage
from pokemon.services.pokemon_service import pokemonService
from pokemon.domaine.equipe import equipe
from pokemon.domaine.client_exception import clientException
class equipeService:
    def __init__(self, equipe_storage_interface, pokemon_service):
        self.equipe_storage_interface = equipe_storage_interface
        self.pokemon_service = pokemon_service 
    def recuperer_equipes(self):
        return self.equipe_storage_interface.recuperer_equipes()
    def recuperer_equipe(self, nom_equipe):
        return self.equipe_storage_interface.recuperer_equipe(nom_equipe)
    def ajouter_pokemon_a_equipe(self, nom_equipe, id_pokemon):
        pokemon_a_ajouter = self.pokemon_service.recuperer_pokemon(id_pokemon)
        try:
            equipe_recuperee = self.recuperer_equipe(nom_equipe)
            equipe_recuperee.ajouter_pokemon_a_equipe(pokemon_a_ajouter)
            self.equipe_storage_interface.ajouter_pokemon_dans_equipe(equipe_recuperee)
        except clientException as client_exception:
            raise clientException