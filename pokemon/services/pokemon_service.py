class pokemonService:
    def __init__(self, pokemon_storage_interface):
        self.pokemon_storage_interface = pokemon_storage_interface
    def recuperer_pokemon(self, position_dans_la_liste):
        return self.pokemon_storage_interface.recuperer_pokemon(position_dans_la_liste)
    def rechercher_pokemon(self, nom_pokemon):
        return self.pokemon_storage_interface.rechercher_pokemon(nom_pokemon)
    def taille_de_la_liste(self):
        return self.pokemon_storage_interface.taille_de_la_liste()