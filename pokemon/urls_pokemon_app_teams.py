from django.urls import path
from . import views
urlpatterns = [
    path('display/all', views.listerEquipes, name="consult_teams"),
    path('display/<str:nom_equipe>', views.afficherEquipe, name="display_team"),
    path('display_pokemon_team_add_list?id_pokemon=<int:id_pokemon>', views.listerEquipesPourAjout, name="lister_equipes_pokemon_pour_ajout")
]