import re
from django import template
from django.template.defaultfilters import stringfilter
register = template.Library()

@stringfilter
@register.filter(name='format_url', is_safe=True)
def format_url(url):
    return re.sub(r"/|\.", r"\\", url)
